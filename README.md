# Views username

Views username module provides field handler for user or author.
Name (raw) views field can display raw username and not truncated.

## Installation

Standard module installation applies. See
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

## Usage

* Add field of type 'Name (raw)' to your views and you can configure
  the label of anonymous user.