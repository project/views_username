<?php
/**
 * Views hooks
 */

/**
 * Implements hook_views_data_alter().
 */
function views_username_views_data_alter(&$data) {
  $data['users']['name_raw'] = array(
    'title' => t('Name (raw)'),
    'help' => t('The raw name of user or author.'),
    'real field' => 'name',
    'field' => array(
      'handler' => 'views_handler_field_user_name_raw',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'title' => t('Name (raw)'),
      'help' => t('The user or author name. This filter does not check if the user exists and allows partial matching. Does not utilize autocomplete.'),
    ),
  );
}