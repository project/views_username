<?php

/**
 * @file
 * Definition of views_handler_field_user_name_raw
 */

class views_handler_field_user_name_raw extends views_handler_field_user {

  /**
   * Default label for anonymous
   *
   * @var string
   */
  private $anonymous;

  /**
   * Override init function to set default label for anonymous.
   */
  public function init(&$view, &$data) {
    parent::init($view, $data);
    $this->anonymous = variable_get('anonymous', t('Anonymous'));
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['overwrite_anonymous'] = array('default' => FALSE, 'bool' => TRUE);
    $options['anonymous_text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    $form['overwrite_anonymous'] = array(
      '#title' => t('Overwrite the value to display for anonymous users'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['overwrite_anonymous']),
      '#description' => t('Enable to display different text for anonymous users.'),
      '#fieldset' => 'more',
    );
    $form['anonymous_text'] = array(
      '#title' => t('Text to display for anonymous users'),
      '#type' => 'textfield',
      '#default_value' => $this->options['anonymous_text'],
      '#dependency' => array(
        'edit-options-overwrite-anonymous' => array(1),
      ),
      '#fieldset' => 'more',
    );

    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->get_value($values);
    $uid = $this->get_value($values, 'uid');

    if (empty($uid)) {
      if (!empty($this->options['overwrite_anonymous'])) {
        $value = $this->options['anonymous_text'];
      }
      else {
        $value = $this->anonymous;
      }
    }

    return $this->render_link($this->sanitize_value($value), $values);
  }
}